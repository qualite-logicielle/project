import os
import logging
import anthropic
from git import Repo
import gitlab

anthropic_api_key = os.getenv("ANTHROPIC_API_KEY")
gitlab_url = os.getenv("CI_SERVER_URL")
gitlab_token = os.getenv("GITLAB_TOKEN")
project_id = os.getenv("CI_PROJECT_ID")
merge_request_iid = os.getenv("CI_MERGE_REQUEST_IID")

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def analyze_code_diff(diff_text):
    try:
        client = anthropic.Anthropic(api_key=anthropic_api_key)
        message = client.messages.create(
            model="claude-3-haiku-20240307",
            max_tokens=512,
            system="Vous êtes un expert en revue de code. Votre tâche est d'analyser les changements de code et de fournir des commentaires constructifs. Concentrez-vous sur la qualité du code, les meilleures pratiques, les bugs potentiels et les améliorations de performance. Soyez concis mais approfondi.",
            messages=[
                {"role": "user", "content": f"Analysez les changements de code suivants et fournissez des suggestions d'amélioration :\n\n{diff_text}"}
            ]
        )
        logger.info(f"Anthropic API response: {message}")
        # Extrayez le texte du premier TextBlock
        if message.content and isinstance(message.content[0], anthropic.types.TextBlock):
            return message.content[0].text
        else:
            logger.warning("Unexpected response format from Anthropic API")
            return None
    except anthropic.AnthropicError as e:
        logger.error(f"Error calling Anthropic API: {e}")
        return None

def get_git_diff(repo_path):
    repo = Repo(repo_path)
    diff_text = repo.git.diff('HEAD~1..HEAD')
    return diff_text

def post_comment_to_merge_request(review):
    try:
        gl = gitlab.Gitlab(gitlab_url, private_token=gitlab_token)
        project = gl.projects.get(project_id)
        mr = project.mergerequests.get(merge_request_iid)
        # Limitez la longueur du commentaire si nécessaire
        max_comment_length = 65535  # Ajustez cette valeur selon les limites de GitLab
        if len(review) > max_comment_length:
            review = review[:max_comment_length-3] + "..."
        mr.notes.create({'body': review})
        logger.info("Successfully posted comment to merge request")
    except Exception as e:
        logger.error(f"Failed to post comment to merge request: {str(e)}")
        raise

def main():
    repo_path = os.getenv("REPO_PATH", ".")
    diff_text = get_git_diff(repo_path)
    
    if diff_text:
        logger.info(f"Git diff: {diff_text[:100]}...")  # Log first 100 characters of diff
        review = analyze_code_diff(diff_text)
        logger.info(f"Review content: {review[:100]}...")  # Log first 100 characters of review
        if review and isinstance(review, str):
            post_comment_to_merge_request(review)
            logger.info("Code review posted as a comment on the merge request.")
        else:
            logger.warning(f"Failed to get review suggestions or invalid review format. Type: {type(review)}")
    else:
        logger.warning("No code changes detected.")

if __name__ == "__main__":
    main()